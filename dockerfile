FROM cypress/included:3.2.0
COPY package.json ./
RUN npm install
COPY . .
ENTRYPOINT [ "/run.sh" ]