# Description
When running in rancher, the nextcloud container will prompt to update when you start the container even though there are no updates. This cypress simple script will press the start update button

# Tested on 
nexcloud:17

# Wont work on/when
- If you run multiple nextcloud containers, since some will have applied the update. This would require inter container communication to apply the update on that new container only and you will have an issue with nextcloud not liking the domain name. For the time being, I dont care about this since I run this in a home environment.

# Usage
- Configure as a normal sidecar container
- Pass the environment variable called CYPRESS_url with your domain name
- Depending on the health config you have setted up, the container will need to restart.

# Example

docker run -it -e CYPRESS_url=nextcloud.mydomain.com nextcloud-update
