context('Actions', () => {
  beforeEach(() => {
    cy.clearLocalStorage()
    cy.clearCookies()
  })

  describe('Start Update', function () {
    it('go to url', () => {
      cy.visit(Cypress.env('url'),{headers:{"Host":"ownfranco.ddns.net"}})
      cy.get('input', {timeout: 10000}).click()
    })
   })
})
